# Efficiently Processing and Merging InfoUSA files

### Author: Alyssa Ting
### Mentors: Celine Robinson, Mark McCahill

## Project Introduction 
The primary goal of this project was to develop a series of sbatch scripts which efficiently processes/merge files in the InfoUSA dataset according to researcher needs. This includes offereing options for spatial subsetting of the data (looking to only pull files from a specific country or state) or subsetting based on a list of zipcodes provided by the researcher.

## About the Data
The InfoUSA dataset contains household-level demographic data, with variables spanning household coordinates, average income, number of children per household, and more. The InfoUSA directory spans multiple different years, and for each year, contains 1 file per zipcode in the US. This dataset can be used to enrich researchers' ability to incorporate demographic data into their work, however it can be time-consuming to process.

## Description of Included Python and Job Files:
This repository contains an sbatch script that calls the Python scripts appropriately in order to perform a merge based on spatial subset of the data or a given textfile of zipcodes.

### ```check_folder.sh```
```check_folder.sh``` is an sbatch script which runs an infinite while loop, and at each iteration of the loop, checks to see if the folder ```json_files``` has a ```.json``` file holding the information for a user's request. If it does, it submits the ```parse_json.sh``` sbatch script to parse the specifications of the user's request.

### ```parse_json.sh```
```parse_json.sh``` is an sbatch script which calls the Python script ```parse_json.py```. ```parse_json.py``` is a Python script which takes in the relative path of the ```.json``` file submitted for the user's request, parses the specifications in the file into arguments, and calls ```merge_per_year.sh``` for each year requested, with the specifications provided.

### ```merge_per_year.sh```
```merge_per_year.sh``` is an sbatch script which calls the Python scripts provided in the file to efficiently merge InfoUSA files based on the researcher's needs (either a spatial subset or a subset based on a provided textfile of zipcodes to pull data for).

* **Spatial subset:** If the researcher would like a spatial subset, this sbatch script will first call Python script ```spatial_subset.py```, which will output (into the output directory) a textfile with the zipcodes that lie within the geometries provided. Then, the script will call ```split.py``` to split the aforementioned zipcode textfile into 1,000 file chunks, and call ```batch_merge.sh``` to execute ```batch_merge.py``` on each of these textfiles. Lastly, the sbatch script will call ```concat.sh``` to execute ```concat.py``` and merge the separate ```.parquet``` files for each chunk into one, final, merged dataframe, and write it out to the output directory as a ```.parquet``` file.
* **Merge based on a provided textfile:** This operates almost exactly like the above. If the researcher would like to pull data for zipcodes specified in a textfile (with one zipcode per line), this sbatch script will skip the spatial subsetting part described above and start by splitting the input zipcode textfile into 1,000 file chunks. It will then continue exactly the same as the processes above, until there is a final, merged dataframe written to the output directory as a ```.parquet``` file.
* **Merge all data for one year:** If the researcher would like data for all zipcodes provided in the dataset for a specific year, this sbatch script will first call Python script ```get_all_zipcodes.py```, which will output (into the output directory) a textfile with all the zipcodes provided for that year. Then, the script continue on like the two above, calling ```split.by```, ```batch_emrge.sh```, and ```concat.sh```. 

### ```spatial_subset.py```
```spatial_subset.py``` is a Python script which uses flags to take in four command-line arguments:
* **--spatial_file:** this flag takes in the spatial_file argument for the script. It must be a complete path to a shapefile containing the geometries the researcher would like to subset by. For example, if the researcher would like to pull InfoUSA data for New York, they would need to provide a shapefile containing ONLY geometries for New York.
* **--zipcode_file:** this flag takes in the zipcode_file argument for the script. It must be a complete path to the shapefile containing the geometries of all zipcodes in the US. Such a file can be found [here](https://www.census.gov/geographies/mapping-files/time-series/geo/carto-boundary-file.html).
* **--infousa_path:** this flag takes in the infousa_path argument for the script. It must be a complete path to the directory containing the InfoUSA files the researcher would like to merge. If they would like to merge files from a specific year, this path must be to that specific year; i.e.: /infousa/2020 instead of simply /infousa.
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will read in the shapefiles provided by the ```spatial_file``` and ```zipcode_file``` arguments. Then, it will use the GeoPandas library's ```sjoin()``` function to create a dataframe only containing geometries in the subsetting spatial file which intersect with the geometries in the zipcode shapefile provided. In other words, it will create a dataframe containing geometries for only the zipcodes within the regions provided by the ```spatial_file``` argument. Then, it will pull the zipcodes from this data frame into a list, and from this list create a textfile with each zipcode per line. This textfile will be written out to the output directory given in ```output_path```.

### ```split.py```
```split.py``` is a Python script which uses flags to take in two command-line arguments:
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--zipcode_input:** this flag takes in the zipcode_input argument for the script. It must be a complete path to the textfile containing the zipcodes to merge the InfoUSA files by. For example, if a researcher would like InfoUSA data on zipcodes 93401, 87620 and 762217, they would make a textfile with one zipcode per line and use the path to that textfile as the argument for this flag.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will take the textfile provided by the ```zipcode_input``` argument and split it into 1,000 zipcode chunks. Each new textfile will have at MOST 1,000 lines, each line one zipcode the researcher would like data from. Each textfile created will be written to a folder named ```split_files_[year]``` in the output directory, where the year comes from input argument ```year```.

### ```batch_merge.py```
```batch_merge.py``` is a Python script which uses flags to take in four command-line arguments: 
* **--zipcode_input:** this flag takes in the zipcode_input argument for the script. It must be a complete path to the textfile containing the zipcodes to merge the InfoUSA files by. For example, if a researcher would like InfoUSA data on zipcodes 93401, 87620 and 762217, they would make a textfile with one zipcode per line and use the path to that textfile as the argument for this flag.
* **--infousa_path:** this flag takes in the infousa_path argument for the script. It must be a complete path to the directory containing the InfoUSA files the researcher would like to merge. If they would like to merge files from a specific year, this path must be to that specific year; i.e.: /infousa/2020 instead of simply /infousa.
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--filename**: this flag takes in the filename argument for the script. It ensures that when ```batch_merge.py``` is called for every 1,000-file chunk, the merged file of those 1000 zipcodes is saved with the appropriate name.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will read in the textfile provided by the ```--zipcode_input``` argument and create a list of zipcodes. Then, it will filter for only zipcodes that are provided in the InfoUSA dataset, and iterate through each zipcode in that list, merging the InfoUSA file for that zipcode, until there is one dataframe which is the merged files for that 1,000 zipcode chunk. The script will then write the merged dataframe to the output directory as a ```.parquet``` file.

### ```concat.py```
```concat.py``` is a Python script which uses flags to take in one command-line argument:
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will look in the output directory for the ```.parquet``` files containing each chunk of 1,000 merged zipcodes, and merge all those files into one, final, merged dataframe. This dataframe will be written to the ```merged_infousa_files_[year]``` folder within the output directory, where the year comes from input argument ```year```.

## Description of Included Jupyter Notebooks:
This repository includes a few Jupyter Notebooks to help you get started and test the code!
* ```test_json_files.ipynb```: walks you through how to create a JSON file with your data merge specifications and how to name it correctly
* ```test_zipcode_textfiles.ipynb```: walks you through how to create a textfile with zipcodes to merge the data by
* ```testing.ipynb```: a Jupyter Notebook included for you to view your merged dataframes

## To run this in your directory:
To run this repository in your own directory and not through the website, do the following:
1. Clone this repository: ```git clone git@gitlab.oit.duke.edu:at341/infousa-processing-scripts.git```
2. Follow the instructions under the ```Requirements``` section to install dependencies.
3. Make the following changes:
* ```check_folder.sh```
    * line 17: ```if [ ! -z "$(ls json_files)" ]; then```
    * line 20: json="json_files/*"      
* ```parse_json.py```:
    * line 36: ```processed_dir = "processed_json_files/"```
    * line 37: ```request_dir = "json_files/"```
    * line 38: ```query_data_dir = "query_data/"```
    * line 81: ```processed_dir = "processed_json_files/"```
    * line 82: ```request_dir = "json_files/"```
* ```concat.py```:
    * line 48: ```query_data_dir = "query_data/"```
4. Use the ```test_json_files.ipynb``` Jupyter Notebook file to create a ```.json``` file with the specifications for your data merge request. Export this JSON file to the empty ```json_files``` sub-folder in the ```bash``` folder.
5. In your DCC terminal, change directory so that you are in the ```infousa-processing-scripts/bash``` directory, and run the command ```sbatch check_folder.sh```. This will run the script, which will complete your data merge request as specified in the JSON file you placed into the ```json_files``` folder. When it is done, your merged dataframe will be in the output directory specified and your JSON file will be moved to the ```processed_json_files``` folder.

Notes:
* Each JSON file you create should be named ```<JobID>.json```
* If you wish to do spatial subsetting, along with a JSON file with specifications for the data merge, you must also provide a ```.zip``` file named ```<JobID>.zip```, containing the ```.shp``` file you'd like to subset by, along with the shapefile dependencies ```.prj```, ```.shx```, ```.dbf```, ```.cpg```. 

## Requirements:
The following packages are required to run this code:
* Geopandas
* Pandas
* Pyarrow
* Fastparquet

One easy way to isntall these packages and use them with these scripts is to create a miniconda environment, which is activated in the ```check_folder.sh``` job script using the line ```source activate <envname>```.

The following steps can be followed to create such an environment and install the necessary packages:
1. ```conda create --name=<envname> python``` creates a miniconda environment with name envname
2. ```conda activate <envname>``` activates the miniconda environment
3. ```conda install --channel conda-forge geopandas``` (must be done first in order to avoid errors)
4. ```conda install pandas``` 
5. ```conda install -c conda-forge pyarrow```
6. ```conda install -c conda-forge fastparquet```

Then, change line 11 in ```check_folder.sh``` to be ```source activate <testenv>```.

Finally, in order for the spatial subsetting option to work, you must change the ```zipcode_path_in``` variable in ```parse_json.py``` (line 53 to ```zipcode_path_in = "us_zipcodes_files/us_zipcodes.shp"```. This path leads to a shapefile containing the geometries of all US zipcodes to this repository, and will allow the ```spatial_subset.py``` file to determine which US zipcodes correspond to the requested region (as the InfoUSA data is stored in text files per zipcode. 