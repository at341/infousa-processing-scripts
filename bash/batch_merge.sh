#!/bin/bash

#SBATCH --job-name=batch_merge               ## Name of the job
#SBATCH --output=batch_merge.out             ## Output file
#SBATCH --error=batch_merge.err              ## Error file
#SBATCH --ntasks=1                           ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=8                    ## The number of threads the code will use
#SBATCH --mem-per-cpu=1200MB                 ## Real memory(MB) per CPU required by the job


python batch_merge.py --zipcode_input $file_name --infousa $infousa_in --output $output_in --filename $file_name --year $year --jobid $jobid --netid $netid

echo "batch merge done." >> batch_merge.out
