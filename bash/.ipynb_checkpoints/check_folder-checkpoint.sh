#!/bin/bash

#SBATCH --job-name=check_folder.sh     ## Name of the job
#SBATCH --output=check_folder.out      ## Output file
#SBATCH --error=check_folder.err       ## Error file
#SBATCH --ntasks=1                     ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=1              ## The number of threads the code will use
#SBATCH --mem-per-cpu=50MB             ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

# activate conda environment
module unload Anaconda3/2021.05
source activate infousa_processing #borsuklab
# start an infinite while loop
while :
do
    # if the json_files folder is not empty, there are requests to submit!
    if [ ! -z "$(ls /hpc/group/borsuklab/infousa_processing_writeable/json_files)" ]; then
    
        # grab all files in the folder
        json="/hpc/group/borsuklab/infousa_processing_writeable/json_files/*" 
        # for every file, if it is a json file, submit the job with that json file
        for file in $json; do
            if [[ $file == *.json ]]
            then
                json_name=$file
                sbatch --export=ALL,json_name=$json_name parse_json.sh
                echo "Submitted parse_json with: " $json_name >> check_folder.out
                sleep 30
            fi
        done
    fi
    sleep 120
done