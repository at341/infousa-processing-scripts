import os
import argparse

def get_args_parse():
    """    
    Uses the argparse library to create an argparse object which accepts 5 required arguments.
    Returns: 
        argparse object.
    """
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    my_parser.add_argument('--infousa', type=str, required=True, 
                           help='a complete path to the InfoUSA dataset')
    my_parser.add_argument('--output', type=str, required=True, 
                           help='a complete path to the folder you\'d like your output to be written to')
    my_parser.add_argument('--year', type=str, required=True, 
                           help='a complete path to the json file to parse into arguments')
    my_parser.add_argument('--jobid', type=str, required=False, 
                       help='JobID')
    my_parser.add_argument('--netid', type=str, required=False, 
                   help='User NetID')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

def get_all_zipcodes(infousa_path, output_path, year, jobid, netid):
    """
    Writes all zipcodes provided in the InfoUSA directory for the given year and writes these zipcodes into a 
    textfile with one zipcode per line and outputs it into the output directory.
    it writes these zipcodes into a textfile with one zipcode per line and outputs it into the output directory.
    Args:
        infousa_path (str): a complete path to the InfoUSA directory
        output_path (str): a complete path to a folder (with writing permissions) for the merged files to be written into
        year (str): the year this spatial subsetting is being done for
        netid (str): user's NetID
        jobid (str): JobID this request belongs to
    """
    
    infousa_path = infousa_path + r"/derived set/" + str(year)
    
    # create a list of all the files in the infousa directory for that year
    all_files = os.listdir(infousa_path)
    
    # create a textfile with one zipcode per line
    with open(str(output_path) + r'/zipcodes_to_merge_' + str(year) + '-' + str(netid) + '-' + str(jobid) + '.txt', 'w') as f:
        for file in all_files:
            zipcode = file[24:-14] 
            f.write(f"{zipcode}\n")
    f.close()
    
    return

if __name__ == '__main__':
    
    args = get_args_parse()
          
    get_all_zipcodes(args.infousa, args.output, args.year, args.jobid, args.netid)
