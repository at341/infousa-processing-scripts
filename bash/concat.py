import pandas as pd
import os
import argparse
from fastparquet import write
import shutil

def get_args_parse():
    """    
    Uses the argparse library to create an argparse object which accepts 7 required arguments.
    Returns: 
        argparse object.
    """
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for concatenating files')
    
    # Add the arguments      
    my_parser.add_argument('--input_dir', type=str, required=True, 
                           help='a complete path to the dir containing file that will be merged')
    my_parser.add_argument('--output_dir', type=str, required=True, 
                           help='a complete path to the dir that will store parquet file')
    my_parser.add_argument('--output_filename', type=str, required=True, 
                           help='the name of the parquet file created')    
    my_parser.add_argument('--year', type=str, required=True, 
                       help='year') 
    my_parser.add_argument('--jobid', type=str, required=True, 
                       help='JobID')
    my_parser.add_argument('--netid', type=str, required=True, 
                   help='User NetID')

    # Execute the parse_args() method
    args = my_parser.parse_args()
    return args

def concatenate_files(input_dir, output_dir, output_filename, year, jobid, netid):
    """
    Takes files in the given directory and concatenates them into a single parquet file, writing them
        to the same folder as specified in the path inputted.
    Args:
        input_dir (str): a complete path to the directory containing all files to concatenate
        output_dir (str): a complete path to the dir that will store parquet file
        output_filename (str): the name of the parquet file created
        year (str): the year this spatial subsetting is being done for
        jobid (str): JobID this request belongs to
        netid (str): user's NetID
    """
    
    query_data_dir = "/hpc/group/borsuklab/infousa_processing_writeable/query_data/"

    # creating a list containing all the filenames of the files to concatenate
    files_list = os.listdir(input_dir + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid))

    # read in the first file as a dataframe
    df = pd.read_parquet(input_dir + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid) + r'/' + files_list[0])    
    df.to_parquet(output_dir + r'/'+ output_filename + r'_' + str(year) + '-' + str(jobid) + '-' + str(netid) +  '.parquet')
    
    # iterate through the remaining files in the directory and appending to existing merged parquet file
    for file in files_list[1:]:
        df = pd.read_parquet(input_dir + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid) + r'/' + file)
        write(output_dir + r'/'+ output_filename + r'_' + str(year) + '-' + str(jobid) + '-' + str(netid) + '.parquet', df, append=True)
    
    # after everything is done, delete all the folders/files created along the way that 
    # are no longer necessary
    shutil.rmtree(input_dir + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid))
    shutil.rmtree(str(output_dir) + '/split_files_' + str(year) + "-" + str(jobid) + "-" + str(netid))
    
    if os.path.exists(query_data_dir + netid + "-" + jobid):
        shutil.rmtree(query_data_dir + netid + "-" + jobid)
    
    if os.path.exists(str(output_dir) + r'/zipcodes_to_merge_' + str(year) + '-' + str(netid) + '-' + str(jobid) + '.txt'):
        os.remove(str(output_dir) + r'/zipcodes_to_merge_' + str(year) + '-' + str(netid) + '-' + str(jobid) + '.txt')

if __name__ == '__main__':
    args = get_args_parse()
    concatenate_files(args.input_dir, args.output_dir, args.output_filename, args.year, args.jobid, args.netid)

                                        
                                        
       