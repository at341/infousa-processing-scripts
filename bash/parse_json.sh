#!/bin/bash

#SBATCH --job-name=parse_json          ## Name of the job
#SBATCH --output=parse_json.out        ## Output file
#SBATCH --error=parse_json.err         ## Error file
#SBATCH --ntasks=1                     ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=1              ## The number of threads the code will use
#SBATCH --mem-per-cpu=200MB            ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

# source activate borsuklab

python parse_json.py --json $json_name