import os
import argparse
import pandas as pd
import shutil
import ntpath
import tempfile
import re
import glob
def get_args_parse():
    """    
    Uses the argparse library to create an argparse object which accepts one required argument.
    Returns: 
        argparse object.
    """
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    my_parser.add_argument('--json', type=str, required=True, 
                           help='a complete path to the json file to parse into arguments')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

def parse_arguments(json_file):
    """
    Takes the json file which contains the user's query specifications and parses it into local 
    variables and submits the merge_per_year.sh job file with these arguments.
    Args:
        json_file (str): the relative path to the json_file to parse
    """
    
    processed_dir = "/hpc/group/borsuklab/infousa_processing_writeable/processed_json_files/"
    request_dir = "/hpc/group/borsuklab/infousa_processing_writeable/json_files/"
    query_data_dir = "/hpc/group/borsuklab/infousa_processing_writeable/query_data/"
    
    json = processed_dir + str(json_file)

    # use pandas to read the json file, then assign local variables
    # to the different specifications in the query request
    df = pd.read_json(json)
    jobid = str(df['JobID'][0])
    netid = str(df['NetID'][0])
    zipcode_in = str(df['zipcode_in'][0])
    spatial_in = str(df['spatial_in'][0])
    output_in = str(df['output_in'][0])
    output_filename = str(df['output_filename'][0])
    years = df['years'].tolist()
    
    zipcode_path_in = "/hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp"
        
    # if there the user would like to do spatial subsetting, first check if there
    # is a zipfile corresponding to the job id with the required files for this subsetting
    if (spatial_in != "NA"):
        if os.path.exists(processed_dir + jobid + ".zip"):
            
            # make a directory to hold the unzipped files, distinguised by job id and user's netid,
            # then unzip files
            os.mkdir(query_data_dir + netid + "-" + jobid)            
            shutil.unpack_archive(processed_dir + jobid + ".zip", query_data_dir + netid + "-" + jobid)
            
            # find the name of the shapefile by finding the only file in the newly created 
            # directory with the .shp extension
            #files = os.listdir(query_data_dir + netid + "-" + jobid)
            #r = re.compile(".*\.shp$")
            #newlist = list(filter(r.match, files)) 
            #spatial_in = query_data_dir + netid + "-" + jobid + r"/" + str(newlist[0])
            spatial_in = glob.glob(query_data_dir + netid + "-" + jobid + "/*" + ".shp")
            print(spatial_in)
    # for every year the user requested data for, submit the merge_per_year.sh job
    # with the user's specifications. Use the --export flag to export the environment
    # variables to the merge_per_year.sh job 
    for year in years:
        os.system("sbatch --export=ALL,zipcode_in=" + zipcode_in + ",infousa_in=/cifs/infousa,output_in=" + output_in + ",spatial_in=" + spatial_in + ",zipcode_path_in=" + str(zipcode_path_in) + ",output_filename=" + output_filename + ",year=" + str(year) + ",jobid=" + jobid + ",netid=" + netid + " merge_per_year.sh")

if __name__ == '__main__':
           
    json = get_args_parse().json
    processed_dir = "/hpc/group/borsuklab/infousa_processing_writeable/processed_json_files/"
    request_dir = "/hpc/group/borsuklab/infousa_processing_writeable/json_files/"
    
    # first, move the json into the processed_json_files folder (so that it
    # doesn't get resubmitted during the next loop of check_folder.sh)
    shutil.move(json, processed_dir)
    
    # get the relative path of the json file of this job
    json = ntpath.basename(json)
    
    # if there exists a .zip file with the same jobid, also move this .zip file to the
    # processed_json_files folder
    if os.path.exists(request_dir + json[:-5] + ".zip"):
        shutil.move(request_dir + json[:-5] + ".zip", processed_dir)

    
    # call the parse_arguments method with this json file
    parse_arguments(json)
    
    
    